package main.java.BL;

import main.java.DO.SystObjects;
import main.java.util.DataLoader;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;

import java.io.IOException;

/**
 * Created by jagan on 6/18/2017.
 */
public final class SysObjectsLoader extends Singleton<SysObjectsLoader> {

    public static Dataset<SystObjects> getSysObjects(DataLoader loader) throws IOException {
        final String query = "(select name, id from sysobjects) T";
        return loader.getData(query).as(Encoders.bean(SystObjects.class));
    }
}
