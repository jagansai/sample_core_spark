package main.java.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Properties {

    @SerializedName("third_ac")
    @Expose
    private Long third_ac;
    @SerializedName("arrival")
    @Expose
    private String arrival;
    @SerializedName("from_station_code")
    @Expose
    private String from_station_code;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("zone")
    @Expose
    private String zone;
    @SerializedName("chair_car")
    @Expose
    private Long chair_car;
    @SerializedName("first_class")
    @Expose
    private Long first_class;
    @SerializedName("duration_m")
    @Expose
    private Long duration_m;
    @SerializedName("sleeper")
    @Expose
    private Long sleeper;
    @SerializedName("from_station_name")
    @Expose
    private String from_station_name;
    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("departure")
    @Expose
    private String departure;
    @SerializedName("return_train")
    @Expose
    private String return_train;
    @SerializedName("to_station_code")
    @Expose
    private String to_station_code;
    @SerializedName("second_ac")
    @Expose
    private Long second_ac;
    @SerializedName("classes")
    @Expose
    private String classes;
    @SerializedName("to_station_name")
    @Expose
    private String to_station_name;
    @SerializedName("duration_h")
    @Expose
    private Long duration_h;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("first_ac")
    @Expose
    private Long first_ac;
    @SerializedName("distance")
    @Expose
    private Long distance;

    public Long getThird_ac() {
        return third_ac;
    }

    public void setThird_ac(Long third_ac) {
        this.third_ac = third_ac;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public String getFrom_station_code() {
        return from_station_code;
    }

    public void setFrom_station_code(String from_station_code) {
        this.from_station_code = from_station_code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public Long getChair_car() {
        return chair_car;
    }

    public void setChair_car(Long chair_car) {
        this.chair_car = chair_car;
    }

    public Long getFirst_class() {
        return first_class;
    }

    public void setFirst_class(Long first_class) {
        this.first_class = first_class;
    }

    public Long getDuration_m() {
        return duration_m;
    }

    public void setDuration_m(Long duration_m) {
        this.duration_m = duration_m;
    }

    public Long getSleeper() {
        return sleeper;
    }

    public void setSleeper(Long sleeper) {
        this.sleeper = sleeper;
    }

    public String getFrom_station_name() {
        return from_station_name;
    }

    public void setFrom_station_name(String from_station_name) {
        this.from_station_name = from_station_name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getReturn_train() {
        return return_train;
    }

    public void setReturn_train(String return_train) {
        this.return_train = return_train;
    }

    public String getTo_station_code() {
        return to_station_code;
    }

    public void setTo_station_code(String to_station_code) {
        this.to_station_code = to_station_code;
    }

    public Long getSecond_ac() {
        return second_ac;
    }

    public void setSecond_ac(Long second_ac) {
        this.second_ac = second_ac;
    }

    public String getClasses() {
        return classes;
    }

    public void setClasses(String classes) {
        this.classes = classes;
    }

    public String getTo_station_name() {
        return to_station_name;
    }

    public void setTo_station_name(String to_station_name) {
        this.to_station_name = to_station_name;
    }

    public Long getDuration_h() {
        return duration_h;
    }

    public void setDuration_h(Long duration_h) {
        this.duration_h = duration_h;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getFirst_ac() {
        return first_ac;
    }

    public void setFirst_ac(Long first_ac) {
        this.first_ac = first_ac;
    }

    public Long getDistance() {
        return distance;
    }

    public void setDistance(Long distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "Properties{" +
                "third_ac=" + third_ac +
                ", arrival='" + arrival + '\'' +
                ", from_station_code='" + from_station_code + '\'' +
                ", name='" + name + '\'' +
                ", zone='" + zone + '\'' +
                ", chair_car=" + chair_car +
                ", first_class=" + first_class +
                ", duration_m=" + duration_m +
                ", sleeper=" + sleeper +
                ", from_station_name='" + from_station_name + '\'' +
                ", number='" + number + '\'' +
                ", departure='" + departure + '\'' +
                ", return_train='" + return_train + '\'' +
                ", to_station_code='" + to_station_code + '\'' +
                ", second_ac=" + second_ac +
                ", classes='" + classes + '\'' +
                ", to_station_name='" + to_station_name + '\'' +
                ", duration_h=" + duration_h +
                ", type='" + type + '\'' +
                ", first_ac=" + first_ac +
                ", distance=" + distance +
                '}';
    }
}