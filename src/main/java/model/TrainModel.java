package main.java.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TrainModel {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("features")
    @Expose
    private Feature features = null;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Feature getFeatures() {
        return features;
    }

    public void setFeatures(Feature features) {
        this.features = features;
    }

    @Override
    public String toString() {
        return "TrainModel{" +
                "type='" + type + '\'' +
                ", features=" + features +
                '}';
    }
}