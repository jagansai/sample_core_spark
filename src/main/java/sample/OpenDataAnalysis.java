package main.java.sample;

import main.java.model.Feature;
import main.java.model.Properties;
import main.java.model.TrainModel;
import main.java.util.DataLoader;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;

import java.io.Serializable;

import static java.lang.System.out;
import static main.java.util.Utils.getResourcePath;

/**
 * Created by jagan on 12-04-2017.
 */


class OpenDataAnalysis implements Serializable {
    private static final Logger logger = Logger.getLogger(OpenDataAnalysis.class.getName());

    public static void main(String[] args)
    {
        logger.info("Started OpenDataAnalysis... ");
        DataLoader loader = null;
        try {
            loader = DataLoader.instance();
       //     convertJSONToJSONLines(getResourcePath("dbresources/trains.json"));
              analyzeTrainsData(loader);
        } catch(Exception ex) {
            out.println(ex.getMessage());
        }finally {
             if (loader != null)
                 loader.getSc().stop();
        }
    }



    private static void analyzeTrainsData(DataLoader loader) {
        /*
        ** Reading from JSON using gson. To read from json file, the format has to be JSON Lines.
        *  In the later version will make the file JSON Lines and read the json from Spark itself.
        */
        final Dataset<TrainModel> trainModelDS = loader.getSession()
                                                       .read()
                                                       .json(getResourcePath("dbresources/trains.jsonl")).as(Encoders.bean(TrainModel.class));
        final Dataset<Feature> featureDS = trainModelDS.map((MapFunction<TrainModel, Feature>) TrainModel::getFeatures, Encoders.bean(Feature.class));
        Dataset<Properties> propertiesDS = featureDS.map((MapFunction<Feature, Properties>) f -> {
            Properties properties = f.getProperties();
            properties.setFrom_station_name(properties.getFrom_station_name().toLowerCase());
            return properties;
        }, Encoders.bean(Properties.class));
        propertiesDS = propertiesDS.cache();
        Dataset<Row> groupTrainsByOrigin = propertiesDS.groupBy(propertiesDS.col("from_station_name")).count();
        final Dataset<Row> groupTrainsByDestination = propertiesDS.groupBy(propertiesDS.col("to_station_name")).count();
        logger.info("Showing top 10 stations from which trains depart");
        groupTrainsByOrigin.orderBy(groupTrainsByOrigin.col("count").desc()).show(10, false);
        groupTrainsByOrigin = groupTrainsByOrigin.cache();
        logger.info("Showing below 10 stations from which trains depat");
        groupTrainsByOrigin.orderBy(groupTrainsByOrigin.col("count")).show(10, false);
        logger.info("Showing top 10 stations to which trains arrive");
        groupTrainsByDestination.orderBy(groupTrainsByDestination.col("count").desc()).show(10, false);
    }
}
