package main.java.util;

import com.google.gson.Gson;
import org.apache.commons.io.FilenameUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.nio.file.Files.readAllLines;

/**
 * Created by jagan on 6/4/2017.
 */
public final class Utils {

    private Utils() {
    }

    public static <T> T fromJSONToObj(String pathToJsonFile, Class<T> cls) throws IOException {
        Gson gson = new Gson();
        String jsonText = readAllLines(Paths.get(pathToJsonFile)).stream().collect(Collectors.joining(""));
        return gson.fromJson(jsonText, cls);
    }

    public static String getResourcePath(String resourceFile) {
        final URL url = Utils.class.getClassLoader().getResource(resourceFile);
        if (url == null)
            throw new InvalidPathException(resourceFile, String.format("Resource: %s not found", resourceFile));
        return url.getPath().substring(1);
    }

    public static void convertJSONToJSONLines(String pathToJSONFile) throws IOException {
        final String baseName = FilenameUtils.getBaseName(pathToJSONFile);
        final String filepath = FilenameUtils.getFullPath(pathToJSONFile);
        final Path outputFilePath = Paths.get(filepath + baseName + ".jsonl");
        try (BufferedReader br = Files.newBufferedReader(Paths.get(pathToJSONFile));
             BufferedWriter writer = Files.newBufferedWriter(outputFilePath)) {
            final int one_kb = 1024;
            final int one_mb = one_kb*1024;
            char[] buf = new char[one_mb];

            final String regexstr = "(\\{\"geometry\": +\\{\"type\": (\"\\w+\", +)\"coordinates\": \\[\\[\\d+\\.\\d+, \\d+\\.\\d+](, \\[\\d+\\" +
                    ".\\d+," +
                    " " +
                    "\\d+\\.\\d+])*]}, +\"type\": (\"\\w+\", +)\"properties\": ?\\{.*?}})";

            final String trainModelPre = "{\"type\": \"FeatureCollection\", \"features\":";
            final Pattern regex = Pattern.compile(regexstr);
            String lastGoemetry = "";
            while(br.read(buf, 0, one_mb) != -1) {
                final String s = lastGoemetry + String.valueOf(buf);
                final Matcher matcher = regex.matcher(s);
                boolean matchFound = false;
                while (matcher.find()) {
                    final String geometry = matcher.group(1);
                    writer.write(trainModelPre + geometry + "}\n");
                    lastGoemetry = geometry;
                    matchFound = true;
                }
                if (matchFound) {
                    lastGoemetry = lastGoemetry.replace(s.substring(s.indexOf(lastGoemetry) + lastGoemetry.length()), "");
                }
                else
                    lastGoemetry = s;
            }
        }
    }
}
