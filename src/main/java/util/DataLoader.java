package main.java.util;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;
import scala.Tuple2;
import scala.collection.JavaConverters;
import scala.collection.Seq;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jagan on 30-04-2017.
 */
public class DataLoader implements Serializable{
    private static JavaSparkContext context_ = null;
    private static SparkSession session_;
    private static final DataLoader ourInstance_ = new DataLoader();

    private static Map<String, String> dbDetails_;
    private static final String propertiesFile = "sample.properties";
    private static final String spark_defaults_conf = "spark-defaults.conf";
    private static SampleProperties properties_ = null;


    public DataLoader() {
        properties_ = SampleProperties.getInstance(propertiesFile, spark_defaults_conf);
        dbDetails_ = new HashMap<>();
        initialise();
    }

    @Override
    protected void finalize() throws Throwable {
        if (context_ != null) {
            context_.close();
        }
        if (session_ != null) {
            session_.close();
        }
    }

    public JavaSparkContext getSc() {
        return context_;
    }

    public SparkSession getSession() { return session_; }

    public static DataLoader instance() {
        return ourInstance_;
    }

    private static String getEnvValueOrDefault(String key, String defaultValue) {
        return dbDetails_.getOrDefault(key, defaultValue);
    }

    public <T extends Object> Dataset<T> createDataSet(List<T> data, Class<T> tClass) {
        return session_.createDataset(data, Encoders.bean(tClass));
    }

    private static String getEnvValue(String key) {
        return getEnvValueOrDefault(key, "");
    }

    private DataLoader initialise() {

        loadEnvironmentVariables();
        createDBProperties();
        String appName = getEnvValueOrDefault("appName", "Sample");
        String master = getEnvValueOrDefault("master", "local[*]");
        SparkConf conf = new SparkConf().setAppName(appName).setMaster(master).setExecutorEnv(getSparkConfVariables());
        context_ = new JavaSparkContext(conf);
        session_ = createSession(appName, master);

        return ourInstance_;
    }

    private static void createDBProperties() {
        Map<String, String> map = new HashMap<>() ;
        map.put("user", getEnvValue("user_name"));
        map.put("password", getEnvValue("password"));

        properties_.putAll(map);
    }

    private static void loadEnvironmentVariables() {
        dbDetails_.putAll(System.getenv());
    }

    private static SparkSession createSession(String appName, String master) {
        return SparkSession.builder()
                           .master(master)
                           .appName(appName)
                           .getOrCreate();
    }


    public Dataset<Row> getData(String table, int fetchSize) {
        final String urlAndDatabaseName = String
                .format("%s;databaseName=%s;", getEnvValue("url"), getEnvValueOrDefault("databaseName",
                                                                                        "master"));
        return session_.read()
                       .format("jdbc")
                       .option("url", urlAndDatabaseName)
                       .option("dbtable", table)
                       .option("user", getEnvValue("user_name"))
                       .option("password", getEnvValue("password"))
                       .option("fetchsize", String.valueOf(fetchSize))
                       .load();
    }

    public static String url() {
        return String.format("%s;databaseName=%s;", getEnvValue("url"), getEnvValueOrDefault("databaseName", "master"));
    }

    public static SampleProperties connectionProperties() {
        return properties_;
    }



    public Dataset<Row> getData(String table) throws IOException {
        return getData(table, 1000);
    }

    public <T> Dataset<T> getData(Class<T> cls) throws IOException {
        return getData(cls, 1000).as(Encoders.bean(cls));
    }

    public <T> Dataset<T> getData(Class<T> cls, int fetchSize) throws IOException {
        return getData(cls.getSimpleName(), fetchSize).as(Encoders.bean(cls));
    }


    public Dataset<Row> getDataFromTempView(String query) {
        SQLContext context = new SQLContext(session_);
        return context.sql(query);
    }

    public Seq<Tuple2<String, String>> getSparkConfVariables() {
        final Enumeration<String> keys = properties_.propertyNames();
        List<Tuple2<String, String>> sparkConfVariables = new ArrayList<>();
        while (keys.hasMoreElements()) {
            String key = keys.nextElement();
            if (key.startsWith("spark.")) {
                final String value = properties_.getValue(key);
                sparkConfVariables.add(new Tuple2<>(key, value));
            }
        }
        return JavaConverters.asScalaIteratorConverter(sparkConfVariables.iterator()).asScala().toSeq();
    }
}
