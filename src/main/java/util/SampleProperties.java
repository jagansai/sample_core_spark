package main.java.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Created by jagan on 30-04-2017.
 */
public class SampleProperties {
    private static final SampleProperties ourInstance_ = new SampleProperties();
    private static final Properties sampleProperties_ = new Properties();
    private static List<String> resourceFiles = null;
    public static SampleProperties getInstance(String... filename) {
        resourceFiles = Arrays.asList(filename);
        return ourInstance_.loadProperties(resourceFiles);
    }

    private SampleProperties() {
    }

    public void putAll(Map<String, String> keyValue) {
        sampleProperties_.putAll(keyValue);
    }

    public Enumeration<String> propertyNames() {
        return (Enumeration<String>)sampleProperties_.propertyNames();
    }

    private SampleProperties loadProperties(List<String> resourceFiles) {
        for (String resourceFile : resourceFiles) {
            try (InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream(resourceFile)) {
                sampleProperties_.load(resourceAsStream);
            }
            catch (IOException ex) {
                // handle exception.
            }
        }
        return this;
    }

    public String getValueOrDefault(String key, String defaultValue) {
        if (sampleProperties_.isEmpty()) {
            loadProperties(resourceFiles);
        }
        return sampleProperties_.getProperty(key, defaultValue);
    }
    public String getValue(String key) {
        return getValueOrDefault(key, "");
    }
}
